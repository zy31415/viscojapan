import numpy as np

from viscojapan.tsana import copy_and_revise_sd_file

from sds import sds
##
##for nsd, sd in enumerate(sds):
##    copy_and_revise_sd_file(
##        '../../../tsana/sea_floor/sd_with_seafloor.h5',
##        'seafloor_sd',
##        'sd_files/sd_with_seafloor_%02d.h5'%nsd,
##        [sd]*3)
   
copy_and_revise_sd_file(
    '../../../tsana/sea_floor/sd_uniform.h5',
    'seafloor_sd',
    'sd_files/sd_uniform.h5',
    [200]*3)
