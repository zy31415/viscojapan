Iter1

Model 0:
VisK - 5.0E17 Pa.s
VisM - 1.0E19 Pa.s
He - 50km
Rake - 90

Model 1 (Variation on VisK):
VisK - 6.0E17 Pa.s
VisM - 1.0E19 Pa.s
He - 50km
Rake - 90

Model 2 (Variation on VisM):
VisK - 5.0E17 Pa.s
VisM - 2.0E19 Pa.s
He - 50km
Rake - 90

Model 3 (Variation on He):
VisK - 5.0E17 Pa.s
VisM - 1.0E19 Pa.s
He - 55km
Rake - 90

Model 4 (Variation on Rake):
VisK - 5.0E17 Pa.s
VisM - 1.0E19 Pa.s
He - 50km
Rake - 80
