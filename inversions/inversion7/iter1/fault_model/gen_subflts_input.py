from viscojapan.pollitz import gen_subflts_input

gen_subflts_input(
    fault_file = 'fault_bott50km.h5',
    out_dir = 'subflts_rake90',
    rake = 90.)

gen_subflts_input(
    fault_file = 'fault_bott50km.h5',
    out_dir = 'subflts_rake95',
    rake = 95.)
