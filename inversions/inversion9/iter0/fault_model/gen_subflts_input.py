from viscojapan.pollitz import gen_subflts_input

gen_subflts_input(
    fault_file = 'fault_bott60km.h5',
    out_dir = 'subflts_bott60km_rake83',
    rake = 83)

gen_subflts_input(
    fault_file = 'fault_bott60km.h5',
    out_dir = 'subflts_bott60km_rake90',
    rake = 90.)
