In this inversion:
1. Use wider fault model that I used in coseismic slip inversion,
 regardless of the elastic depth.
2. Combine coseismic and postseismic displacement;
3. Use smaller SD (larger weighting) for coseismic observation;
4. Horizontal SD : Vertical SD = 1:5
5. Use near field stations only.

#########################################
##  The first interation, iter0:

Model Zero - The original model:
(1) viscosity - 6.3E+18 Pa.s
(2) elastic depth - 50km
(3) rake - 83.

Model One - Variation on viscosity:
(1) viscosity - 1.0E+19 Pa.s
(2) elastic depth - 50km
(3) rake - 83.

Model Two - Variation on elastic depth:
(1) viscosity - 6.3E+18 Pa.s
(2) elastic depth - 40km
(3) rake - 83.

Model Three - Variation on rake:
(1) viscosity - 6.3E+18 Pa.s
(2) elastic depth - 50km
(3) rake - 90.

Nov. 5, 2014


#########################################
##  linear inversion
